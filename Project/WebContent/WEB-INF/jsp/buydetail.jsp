<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>マイページ</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/cart.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="index">Log out</a></li>
            </ul>
        </nav>
    </header>
    <div class="table-control">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">商品名</th>
                    <th scope="col">合計金額</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="ItemdetaBeans" items="${buyItemDetail}" >
                <tr>
                    <th scope="row">${ItemdetaBeans.itemname}</th>
                    <td>${ItemdetaBeans.itemprice}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body>

</html>
