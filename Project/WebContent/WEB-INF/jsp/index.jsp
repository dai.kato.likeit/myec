<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>一覧画面</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1>Amazooon</h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <c:if test="${userinfomation.loginid == 'admin' }"><li><a href="adminindex">admin</a></li></c:if>
            </ul>
        </nav>
    </header>
    <div class="search">

        <form class="form-inline md-form form-sm mt-0" action="searchresult" method="get">
            <i class="fas fa-search" aria-hidden="true"></i>
            <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="What do you want ?" aria-label="Search" name="searchword">
            <button type="submit" class="btn btn-primary">検索</button>
			<div class="tagsearch">
       			 <select class="custom-select" name="categorysearch">
                    <option selected>カテゴリー検索</option>
                    <option value="1">家電</option>
                    <option value="2">家具</option>
                    <option value="3">衣類</option>
                </select>
            </div>
          </form>
		</div>
    <div class="word">
        <h2>New arrival</h2>
    </div>

    <div class="container">
     	<c:forEach var="ItemdetaBeans" items="${itemdeta}">
        <div class="item"><a href="itemdetail?item_id=${ItemdetaBeans.id}"><img src="img/${ItemdetaBeans.filename}"></a>
            <p>${ItemdetaBeans.itemname}</p>
            <p>${ItemdetaBeans.itemprice}円(税込)</p>
        </div>
        </c:forEach>
    </div>

    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body></html>
