<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>商品詳細</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="adminindex">admin</a></li>
            </ul>
        </nav>
    </header>
    <div class="itemfont">
        <h1>about this item</h1>
    </div>
    <div class="itemimage">

        <h2>${iteminfo.itemname}</h2>
        <img src="img/${iteminfo.filename}">
    </div>
    <div class="itemdetail">
    <p>${iteminfo.itemdetail}</p>
    <p>${iteminfo.itemprice}円(税込)</p>
    </div>
    <div class="cartbotan">
    	<form action="cart" method="POST">
    	<input type="hidden" name="item_id" value="${iteminfo.id}">
        <a href="cart?item_id=${iteminfo.id}" class="cartbotandetail"><button type="submit" class="btn btn-danger">カートに入れる</button></a>
        </form>
        <a href="index" class="cartbotandetail"><button type="button" class="btn btn-danger">戻る</button></a>
    </div>
    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body>

</html>
