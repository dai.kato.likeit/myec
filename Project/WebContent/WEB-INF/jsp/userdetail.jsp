<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>マイページ</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/cart.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="logout">Logout</a>
            </ul>
        </nav>
    </header>
    <div class="table-control">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ユーザー名</th>
                    <th scope="col">ログインID</th>
                    <th scope="col">住所</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">${userinfomation.name}</th>
                    <td>${userinfomation.loginid}</td>
                    <td>${userinfomation.adress}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="controall">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">購入詳細</th>
                    <th scope="col">価格(送料込)</th>
                    <th scope="col">購入日時</th>
                </tr>
            </thead>
            <tbody>
            	<!-- ここにforeachを書く -->
				<c:forEach var="BuydataBeans" items="${userbuylist}" >
                <tr>
                    <th scope="row"><a class="btn btn-primary" href="buydetail?id=${BuydataBeans.id}" role="button">詳細</a></th>
                    <td>${BuydataBeans.totalprice}</td>
                    <td>2020年xx月xx日</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    </body>

</html>
