<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>商品追加画面</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="adminindex">admin</a></li>
            </ul>
        </nav>
    </header>
    <div class="adminfont">
        <h1>Administrator rights</h1>
    </div>
    <div class="admincontainer">
        <form action="itemadd" method="post" name="newitem" enctype="multipart/form-data">
            <div class="itemwordfont">商品名</div>
            <input type="text" name="itemname" class="itemword" placeholder="商品名を入力してください">
            <div class="itempricefont">価格</div>
            <input type="text" name="itemprice" class="itempriceword" placeholder="価格を入力してください">
            <div class="tagsearchs">
                <select class="custom-select" name="categorlist">
                    <option selected>カテゴリー名を選択してください</option>
                    <option value="1">家電</option>
                    <option value="2">家具</option>
                    <option value="3">衣類</option>
                </select>
            </div>
            <div class="adminitemimage"><input type=file name="itemimage" class="filephoto"></div>
            <div class="itemdetailfont">商品詳細</div>
            <div class="admintext"><textarea name="itemdetail" placeholder="商品の詳細を入力して下さい" class="admintextarea"></textarea></div>
            <div class="adminbotan"><button type="submit" class="btn btn-secondary">商品登録</button></div>
        </form>
    </div>

    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body>

</html>
