<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>一覧画面</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/cart.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
            </ul>
        </nav>
    </header>
    <h1 class="buyfont">Purchase confirmation</h1>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">商品名</th>
                    <th scope="col">単価</th>
                </tr>
            </thead>
            <tbody>
            	<c:forEach var="iteminfo" items="${cart}">
                <tr>
                    <th scope="row">${iteminfo.itemname}</th>
                    <td>${iteminfo.itemprice}円</td>
				</tr>
                </c:forEach>
                <tr>
                    <th scope="row">${bdb.deliveryMethodName}</th>
                    <td>${bdb.deliveryMethodPrice}円</td>
                </tr>
                <tr>
                	<th scope="row">合計金額</th>
                	 <td>${bdb.totalprice}円</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="buybotan">
    <button type="button" class="btn btn-danger"><a href="userdetail">購入</a></button>
    </div>
    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body>

</html>
