<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>管理者画面</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="admin">admin</a></li>
            </ul>
        </nav>
    </header>
    <div class="adminindexlist">
        <h1>管理者画面</h1>
    </div>
     <div class="adminindexlists">
                <h2><a href="itemadd">商品追加</a></h2>
    </div>


     <div class="controall">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">商品名</th>
                    <th scope="col">価格</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="ItemdetaBeans" items="${itemlist}" >
                <tr>
                    <th scope="row">${ItemdetaBeans.itemname}</th>
                    <td>${ItemdetaBeans.itemprice}円</td>
                    <td>
                        <a class="btn btn-primary" href="adminedit?item_id=${ItemdetaBeans.id}" role="button">編集</a>
                        <a class="btn btn-danger" href="itemdelete?item_id=${ItemdetaBeans.id}" role="button">削除</a>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    </body>
</html>