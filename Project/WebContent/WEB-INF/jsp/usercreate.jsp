<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>新規登録</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="adminindex">admin</a></li>
            </ul>
        </nav>
    </header>
    <div class="login word">
    <h2>Member registration</h2>
    </div>
    <div class="login-form">
        <form action="usercreate" method="post" name="login-form">
            <h3 class="form-word"> Login Id</h3>
        <input type="text" name="loginid" class="form-control" placeholder="ログインIDを入力してください">
        	<h3 class="form-word"> User Name</h3>
        <input type="text" name="username" class="form-control" placeholder="ユーザー名を入力してください">
            <h3 class="form-word">password</h3>
        <input type="password" name="password" class="form-control"  placeholder="パスワードを入力してください">
            <h3 class="form-word">Adress</h3>
        <input type="text" name="address" class="form-control" placeholder="住所を入力してください">
            <div class="form-botan">
            <!-- ボタンのタイプをsubmitに指定にしないと動かない -->
            <button type="submit" class="btn btn-warning">登録</button>
            </div>
        </form>
    </div>
    <footer>
        <div class="wrapper">
            <p><small>&copy; 2020 dai kato</small></p>
        </div>
    </footer>
</body>

</html>
