<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>一覧画面</title>
    <!-- bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- オリジナルのcssの読み込み -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="page-header">
        <h1><a href="index">Amazooon</a></h1>
        <nav>
            <ul class="main-nav">
                <li><a href="login">Login</a></li>
                <li><a href="cart">Cart</a></li>
                <li><a href="userdetail">My page</a></li>
                <li><a href="adminindex">admin</a></li>
            </ul>
        </nav>
    </header>
    <div class="admineditfont">
        <h1>商品編集画面</h1>
    </div>
    <div class="admincontainer">
        <form action="adminedit" method="post" name="newitem">
        	<!-- hiddenパラメターはformタグの下ならどこでも良い -->
            <input type="hidden" name="id" value="${itemedit.id}">
            <div class="itemwordfont">商品名</div>
            ${itemedit.itemname}
            <input type="text" name="itemnameedit" class="itemword" placeholder="商品名を入力してください">
            <div class="itempricefont">価格</div>
            ${itemedit.itemprice}
            <input type="text" name="itempriceedit" class="itempriceword" placeholder="価格を入力してください">
            <div class="itemdetailfont">商品詳細</div>
            ${itemedit.itemdetail}
            <div class="admintext"><textarea name="itemdetailedit" placeholder="商品の詳細を入力して下さい" class="admintextarea"></textarea></div>
            <div class="adminbotan"><button type="submit" class="btn btn-secondary">変更</button></div>
        </form>
    </div>

    </body>
</html>