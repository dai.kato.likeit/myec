package beans;

import java.io.Serializable;

public class CategoryTableDataBeans implements Serializable{
	private int id;
	private String categoryname;

	public CategoryTableDataBeans (int id,String categoryname){
		this.id = id;
		this.categoryname = categoryname;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}


}
