package beans;

import java.io.Serializable;

public class UserdetaBeans implements Serializable{

	private int id;
	private String name;
	private String adress;
	private String loginid;
	private String loginpassword;

	public UserdetaBeans () {

	}

	//コンストラクタ
	public UserdetaBeans(int id,String name, String adress,String loginid,String loginpassword) {
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.loginid = loginid;
		this.loginpassword = loginpassword;
	}

	public UserdetaBeans(String loginIdData,String nameData,String adressData) {
		this.loginid = loginIdData;
		this.name = nameData;
		this.adress = adressData;
	}

	public UserdetaBeans(String loginIdData,String nameData,String adressData,int userid) {
		this.loginid = loginIdData;
		this.name = nameData;
		this.adress = adressData;
		this.id = userid;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getLoginpassword() {
		return loginpassword;
	}
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}
}
