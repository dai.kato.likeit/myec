package beans;

import java.io.Serializable;

public class ItemdetaBeans implements Serializable{
	private int id;
	private String itemname;
	private String itemdetail;
	private int categoryid;
	private int itemprice;
	private String filename;

	public ItemdetaBeans (int id,String itemname,String itemdetail,int categoryid,int itemprice,String filename) {
		this.id = id;
		this.itemname = itemname;
		this.itemdetail = itemdetail;
		this.categoryid = categoryid;
		this.itemprice = itemprice;
		this.filename = filename;
	}
	//商品検索の為のコンストラクタ
		public ItemdetaBeans (int itemidDeta,String itemnameData,String itemdetailDeta,int itempriceDeta,String filenameDeta) {
		this.id = itemidDeta;
		this.itemname = itemnameData;
		this.itemdetail = itemdetailDeta;
		this.itemprice = itempriceDeta;
		this.filename = filenameDeta;
	}

	public ItemdetaBeans(int id,String name,int price) {
			// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.itemname = name;
		this.itemprice = price;
		}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getItemdetail() {
		return itemdetail;
	}
	public void setItemdetail(String itemdetail) {
		this.itemdetail = itemdetail;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public int getItemprice() {
		return itemprice;
	}
	public void setItemprice(int itemprice) {
		this.itemprice = itemprice;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}


}
