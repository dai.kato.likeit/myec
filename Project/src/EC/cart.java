package EC;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemdetaBeans;

/**
 * Servlet implementation class cart
 */
@WebServlet("/cart")
public class cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public cart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();



		ArrayList<ItemdetaBeans> cart = (ArrayList<ItemdetaBeans>) session.getAttribute("cart");
		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemdetaBeans>();
			session.setAttribute("cart", cart);
		}

		// リクエストパラメータの文字コードを指定
		//request.setCharacterEncoding("UTF-8");

		// itemdetail.jspからのパラメータをここで受け取っている
		// String itemids = request.getParameter("item_id");

		// 引数に渡してメソッドを実行
		//ItemdetaBeans cartitem = ItemDAO.getItemByItemID(itemids);

		// セッションスコープにユーザの情報をセット
		//session.setAttribute("cartitem",cartitem);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
