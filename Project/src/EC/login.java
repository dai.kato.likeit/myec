package EC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDAO;
import beans.UserdetaBeans;

/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				//フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//パラメーターの受け取り文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//パラメータの受け取り
		String loginId = request.getParameter("loginid");
		String userPassword = request.getParameter("password");

		//パラメータを引数に渡してログインを実行
		UserdetaBeans userinfo = UserDAO.loginbyId(loginId, userPassword);

				//該当のデータが見つからなかった場合
				if(userinfo == null) {
					//リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "ログインに失敗しました。");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
					dispatcher.forward(request, response);
					return;
				}

				/** テーブルに該当のデータが見つかった場合 **/
				// セッションにユーザの情報をセット
				HttpSession session = request.getSession();
				session.setAttribute("userinfomation", userinfo);


				// リダイレクト
			    response.sendRedirect("index");


	}

}
