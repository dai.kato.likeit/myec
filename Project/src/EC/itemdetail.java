package EC;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ItemDAO;
import beans.ItemdetaBeans;

/**
 * Servlet implementation class itemdetail
 */
@WebServlet("/itemdetail")
public class itemdetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public itemdetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//購入する際にログインできていなかったらログイン画面にリダイレクトさせる処理
			if (session.getAttribute("userinfomation") == null) {
				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
			}

		// リクエストパラメータの文字コードを指定
	    request.setCharacterEncoding("UTF-8");

		//パラメータの受け取り
		String itemid = request.getParameter("item_id");

		//引数でitemidを渡す
		//戻り値を受け取る
		ItemdetaBeans iteminfo = ItemDAO.getItemByItemID(itemid);

		//リクエストパラメータにセット
		request.setAttribute("iteminfo", iteminfo);

		//カートを取得
		ArrayList<ItemdetaBeans> cart = (ArrayList<ItemdetaBeans>) session.getAttribute("cart");

		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemdetaBeans>();
		}

		//カートに商品を追加。
		cart.add(iteminfo);
		//カート情報更新
		session.setAttribute("cart", cart);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}




	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
