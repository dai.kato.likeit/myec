package EC;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.BuyDAO;
import beans.BuyDetaDetailBeans;
import beans.BuydataBeans;
import beans.ItemdetaBeans;
import beans.UserdetaBeans;

/**
 * Servlet implementation class userdetail
 */
@WebServlet("/userdetail")
public class userdetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userdetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		//買い物かご
		ArrayList<ItemdetaBeans> cart = (ArrayList<ItemdetaBeans>) session.getAttribute("cart");
		if(cart != null) {
		BuydataBeans bdb = (BuydataBeans) session.getAttribute("bdb");
		//購入情報の登録処理
		int buyid = BuyDAO.insertBuy(bdb);

		// 購入詳細情報を購入情報IDに紐づけして登録
		for (ItemdetaBeans iteminfo : cart) {
			BuyDetaDetailBeans bddb = new BuyDetaDetailBeans();
			bddb.setBuyId(buyid);
			bddb.setItemId(iteminfo.getId());
			DAO.BuyDetailDAO.insertBuyDetail(bddb);
			}
		}
		//データベースからidを取得するために作ったインスタンス
		UserdetaBeans user = (UserdetaBeans)session.getAttribute("userinfomation");

		ArrayList<BuydataBeans> userbuylist = BuyDAO.getBuyDataBeansListByuserId(user.getId());



		// 情報をリクエストスコープに詰めてJSPに渡す
		request.setAttribute("userbuylist",userbuylist);

		session.removeAttribute("cart");

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
