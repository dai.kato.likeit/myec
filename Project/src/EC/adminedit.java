package EC;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ItemDAO;
import beans.ItemdetaBeans;

/**
 * Servlet implementation class adminedit
 */
@WebServlet("/adminedit")
public class adminedit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminedit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // adminedit.jspからのパラメータをここで受け取っている
        String itemid = request.getParameter("item_id");

        //変数itemidをgetItemByItemIDに渡す
        ItemdetaBeans itemedit = ItemDAO.getItemByItemID(itemid);

        // セッションスコープにユーザの情報をセット
        HttpSession session = request.getSession();
     	session.setAttribute("itemedit",itemedit);

		 } catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}



		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminedit.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コードを指定
    	request.setCharacterEncoding("UTF-8");

    	//パラメータの受け取り
    	String id = request.getParameter("id");
    	String itemname = request.getParameter("itemnameedit");
    	String itemprice = request.getParameter("itempriceedit");
    	String itemdetail = request.getParameter("itemdetailedit");

    	//パラメターを引数に渡す
    	ItemDAO.updateItem(id,itemname,itemprice,itemdetail);

    	// リダイレクト
	    response.sendRedirect("adminindex");
	}

}
