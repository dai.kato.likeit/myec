package EC;

import java.util.ArrayList;

import beans.ItemdetaBeans;

public class my_ec_helper {

	/**
	 * 商品の合計金額を算出する
	 *
	 * @param items
	 * @return total
	 */
	//ここに配送料金が追加されていないので追加する、どう追加するか?
	public static int getTotalItemPrice(ArrayList<ItemdetaBeans> items) {
		int total = 0;
		for (ItemdetaBeans item :items) {
			total += item.getItemprice();
		}
		return total;
	}
}
