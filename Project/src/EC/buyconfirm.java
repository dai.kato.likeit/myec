package EC;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.DeliveryMethodDAO;
import beans.BuydataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemdetaBeans;
import beans.UserdetaBeans;

/**
 * Servlet implementation class buyconfirm
 */
@WebServlet("/buyconfirm")
public class buyconfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public buyconfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		//選択された配送方法を取得
		String deliverymethoid = request.getParameter("delivarymethod");

		//変数を引数に渡してDAO実行(IDから配送方法を取得する)
		DeliveryMethodDataBeans userselectdmd =	DeliveryMethodDAO.getDeliveryMethodDataBeansByID(deliverymethoid);

		//買い物かご
		ArrayList<ItemdetaBeans> cart = (ArrayList<ItemdetaBeans>) session.getAttribute("cart");

		//合計金額
		int totalprice = my_ec_helper.getTotalItemPrice(cart);

		//データベースからidを取得するために作ったインスタンス
		UserdetaBeans userid = (UserdetaBeans)session.getAttribute("userinfomation");

		//beansに情報をセットする作業
		BuydataBeans bdb = new BuydataBeans();
		bdb.setUserid(userid.getId());
		bdb.setDeliverymethodid(userselectdmd.getId());
		bdb.setDeliveryMethodName(userselectdmd.getName());
		bdb.setDeliveryMethodPrice(userselectdmd.getPrice());
		bdb.setTotalprice(totalprice += userselectdmd.getPrice());

		//beansの情報をセッションスコープに詰める
		session.setAttribute("bdb", bdb);

		//buy.jspにフォワードで飛ばす
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp");
		dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
