package EC;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ItemDAO;
import beans.ItemdetaBeans;

/**
 * Servlet implementation class searchresult
 */
@WebServlet("/searchresult")
public class searchresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public searchresult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			//パラメーターの受け取り文字コードを指定
			request.setCharacterEncoding("UTF-8");

			//パラメータの受け取り
			String searchword =	request.getParameter("searchword");
			System.out.println(searchword);

			//カテゴリー検索のパラメータの受け取り
			String categoryid = request.getParameter("categorysearch");

			//searchwordを引数にして検索するメソッドをDAOで実行
			ArrayList<ItemdetaBeans> searchresult = ItemDAO.getItembysearchword(searchword,categoryid);
			request.setAttribute("searchresult", searchresult);

			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchresult.jsp");
			dispatcher.forward(request, response);





	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
