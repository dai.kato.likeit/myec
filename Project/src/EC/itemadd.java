package EC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import DAO.ItemDAO;

/**
 * Servlet implementation class itemadd
 */
@WebServlet("/itemadd")
@MultipartConfig(location="/Users/katoudai/Documents/myec/Project/WebContent/img", maxFileSize=1048576)
public class itemadd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public itemadd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				//フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemadd.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			//リクエストパラメータの文字コードを指定
    		request.setCharacterEncoding("UTF-8");

    		//パラメータの受け取り
    		String Insertitemname = request.getParameter("itemname");
    		String Insertitemprice = request.getParameter("itemprice");
    		String Insertcategoryid = request.getParameter("categorlist");
    		Part Insertitemimage = request.getPart("itemimage");
    		String imameName = getFileName(Insertitemimage);
    		String Insertitemdetail = request.getParameter("itemdetail");

    		Insertitemimage.write(imameName);

    		//パラメーターを引数にDAOを呼び出す
    		ItemDAO.insertItem(Insertitemname,Insertitemprice,Insertcategoryid, imameName,Insertitemdetail);

    		// リダイレクト
    	    response.sendRedirect("adminindex");
	}

	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }

}
