package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemdetaBeans;

public class ItemDAO {
			//index.jspに商品を出す為のメソッド
			public static ArrayList<ItemdetaBeans> getItemtoindex (int limit){
			Connection con = null;
			ArrayList <ItemdetaBeans> itemlist = new ArrayList<ItemdetaBeans>();
			PreparedStatement st = null;
			try {


				//DBに接続
				con = DBManager.getConnection();

				//SQL文を用意
				st = con.prepareStatement("select * from m_item order by id desc limit ?");

				st.setInt(1, limit);
				//実行は一番最後に置くsetの前においてはいけない
				ResultSet rs = st.executeQuery();

				while(rs.next()) {
					int id = rs.getInt("id");
					String itemname = rs.getString("item_name");
					String itemdetail = rs.getString("item_detail");
					int categoryid = rs.getInt("category_id");
					int itemprice = rs.getInt("item_price");
					String filename = rs.getString("file_name");
					ItemdetaBeans item = new ItemdetaBeans(id,itemname,itemdetail,categoryid,itemprice,filename);
					itemlist.add(item);

				} return itemlist;
				}catch (SQLException e) {
				e.printStackTrace();
	            return null;
				}finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
		}

	//商品検索の為のメソッド(部分一致検索)
	public static ArrayList<ItemdetaBeans> getItembysearchword (String searchword,String categorysearchId) {
		Connection con = null;
		ArrayList <ItemdetaBeans> itemlist = new ArrayList<ItemdetaBeans>();
		try {


			//DBに接続
			con = DBManager.getConnection();

			//SQL文を用意
			String sql = "select * from m_item where item_name like ?";

			//categoryid検索する為に結合を行う
			if (!categorysearchId.equals("")) {
				sql += " and category_id = '" + categorysearchId + "'";
			}

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,"%"+searchword+"%");

			ResultSet rs = ps.executeQuery();


			while(rs.next()) {
				int id = rs.getInt("id");
				String itemname = rs.getString("item_name");
				String itemdetail = rs.getString("item_detail");
				int categoryid = rs.getInt("category_id");
				int itemprice = rs.getInt("item_price");
				String filename = rs.getString("file_name");
				ItemdetaBeans item = new ItemdetaBeans(id,itemname,itemdetail,categoryid,itemprice,filename);
				itemlist.add(item);

			} return itemlist;
			}catch (SQLException e) {
			e.printStackTrace();
            return null;
			}finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}


	//商品IDによる商品検索
	public static ItemdetaBeans getItemByItemID(String itemId) throws SQLException {
	Connection con = null;
	try {
		// データベースへ接続
        con = DBManager.getConnection();

        //select文を実行
        String sql = "SELECT * FROM m_item WHERE id = ?";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, itemId);
        ResultSet rs = ps.executeQuery();

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int id = rs.getInt("id");
		String itemname = rs.getString("item_name");
		String itemdetail = rs.getString("item_detail");
		int categoryid = rs.getInt("category_id");
		int itemprice = rs.getInt("item_price");
		String filename = rs.getString("file_name");
        return new ItemdetaBeans(id,itemname,itemdetail,categoryid,itemprice,filename);

	 } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


	//管理者画面に商品を全部出す為のメソッド
	public static ArrayList<ItemdetaBeans> findallitem(){
	Connection con = null;
	ArrayList <ItemdetaBeans> itemlist = new ArrayList<ItemdetaBeans>();
	try {


	//DBに接続
	con = DBManager.getConnection();

	//SQL文を用意
	String sql = "select * from m_item";


	//実行は一番最後に置くsetの前においてはいけない
	Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(sql);


	while(rs.next()) {
		int id = rs.getInt("id");
		String itemname = rs.getString("item_name");
		String itemdetail = rs.getString("item_detail");
		int categoryid = rs.getInt("category_id");
		int itemprice = rs.getInt("item_price");
		String filename = rs.getString("file_name");
		ItemdetaBeans item = new ItemdetaBeans(id,itemname,itemdetail,categoryid,itemprice,filename);
		itemlist.add(item);

	} return itemlist;
	}catch (SQLException e) {
	e.printStackTrace();
    return null;
	}finally {
    // データベース切断
    if (con != null) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
   }
 }


	//商品編集の為のメソッド
	public static void updateItem(String id,String itemname,String itemprice,String itemdetail) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        // UPDATE文の実行
        String sql = "update m_item set item_name = ?,item_price = ?,item_detail = ? where id = ?";
        //PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);

        ps.setString(1, itemname);
        ps.setString(2, itemprice);
        ps.setString(3, itemdetail);
        ps.setString(4, id);

        ps.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();

		}	finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
	}


	//商品登録の為のメソッド
	public static void insertItem(String Insertitemname,String Insertitemprice,String Insertcategoryid,String Insertitemimage,String Insertitemdetail) {
	Connection conn = null;
	try {
		 // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文でデータを入れる
      String sql = "insert m_item (item_name,item_detail,category_id,item_price,file_name) values (?,?,?,?,?);";
      //PreparedStatement
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, Insertitemname);
      ps.setString(2, Insertitemdetail);
      ps.setString(3, Insertcategoryid);
      ps.setString(4, Insertitemprice);
      ps.setString(5, Insertitemimage);


     ps.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
          // データベース切断
          if (conn != null) {
              try {
                  conn.close();
              } catch (SQLException e) {
                  e.printStackTrace();
              }
          }
		}
	}


	//商品削除の為のメソッド
	public static void deleteitem(String id) {
	Connection conn = null;
	try {
		// データベースへ接続
        conn = DBManager.getConnection();

        //DELETE文の実行
        String sql = "DELETE FROM m_item WHERE id = ?";

        //PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);

        ps.setString(1, id);
        ps.executeUpdate();

	}catch (SQLException e) {
		e.printStackTrace();

	}	finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
     }
  }


  	//カテゴリー検索の為のメソッド
	public static ItemdetaBeans getItemBycategoryId(String categoryid2) throws SQLException {
	Connection con = null;
	try {
		// データベースへ接続
      con = DBManager.getConnection();

      //select文を実行
      String sql = "select * from m_item where category_id = ?";

      PreparedStatement ps = con.prepareStatement(sql);
      ps.setString(1, categoryid2);
      ResultSet rs = ps.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
          return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id");
		String itemname = rs.getString("item_name");
		String itemdetail = rs.getString("item_detail");
		int categoryid = rs.getInt("category_id");
		int itemprice = rs.getInt("item_price");
		String filename = rs.getString("file_name");
      return new ItemdetaBeans(id,itemname,itemdetail,categoryid,itemprice,filename);

	 } catch (SQLException e) {
          e.printStackTrace();
          return null;
      } finally {
          // データベース切断
          if (con != null) {
              try {
                  con.close();
              } catch (SQLException e) {
                  e.printStackTrace();
                  return null;
              }
          }
      }
  }
}
