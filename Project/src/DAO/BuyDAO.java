package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BuydataBeans;


//購入履歴の登録処理を行う為のメソッド
public class BuyDAO {
	public static int insertBuy(BuydataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getUserid());
			st.setInt(2, bdb.getTotalprice());
			st.setInt(3, bdb.getDeliverymethodid());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
 	user_idから情報を取得する為のメソッド
	 */
	public static ArrayList<BuydataBeans> getBuyDataBeansListByuserId(int userid) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					//結合?
					"SELECT * FROM t_buy"
							+ " JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.user_id = ?");
			st.setInt(1, userid);

			ResultSet rs = st.executeQuery();
			ArrayList<BuydataBeans> buyDetailList2 = new ArrayList<BuydataBeans>();

			while (rs.next()) {
				BuydataBeans bdb2 = new BuydataBeans();
				bdb2.setUserid(rs.getInt("user_id"));
				bdb2.setTotalprice(rs.getInt("total_price"));
				bdb2.setCreatedate(rs.getDate("create_date"));
				bdb2.setDeliverymethodid(rs.getInt("delivery_method_id"));
				bdb2.setId(rs.getInt("id"));
				bdb2.setDeliveryMethodPrice(rs.getInt("price"));
				bdb2.setDeliveryMethodName(rs.getString("name"));
				buyDetailList2.add(bdb2);
			}


			return buyDetailList2;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

/**
 * 購入IDによる購入情報検索
 * @param buyId
 * @return BuyDataBeans
 * 				購入情報のデータを持つJavaBeansのリスト
 * @throws SQLException
 * 				呼び出し元にスローさせるため
 */
public static BuydataBeans getBuyDataBeansByBuyId(int buyId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement(
				"SELECT * FROM t_buy"
						+ " JOIN m_delivery_method"
						+ " ON t_buy.delivery_method_id = m_delivery_method.id"
						+ " WHERE t_buy.id = ?");
		st.setInt(1, buyId);


		ResultSet rs = st.executeQuery();

		BuydataBeans bdb = new BuydataBeans();
		if (rs.next()) {
			bdb.setId(rs.getInt("id"));
			bdb.setTotalprice(rs.getInt("total_price"));
			bdb.setCreatedate(rs.getDate("create_date"));
			bdb.setDeliverymethodid(rs.getInt("delivery_method_id"));
			bdb.setUserid(rs.getInt("user_id"));
			bdb.setDeliveryMethodPrice(rs.getInt("price"));
			bdb.setDeliveryMethodName(rs.getString("name"));
		}

		System.out.println("searching BuyDataBeans by buyID has been completed");

		return bdb;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
 }
}

