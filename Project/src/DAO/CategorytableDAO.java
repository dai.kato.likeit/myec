package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.CategoryTableDataBeans;

public class CategorytableDAO {
		//カテゴリーを全部出す為のメソッド
		public static ArrayList<CategoryTableDataBeans> findacategory(){
		Connection con = null;
		ArrayList <CategoryTableDataBeans> categorylist = new ArrayList<CategoryTableDataBeans>();
		try {
		//DBに接続
		con = DBManager.getConnection();

		//SQL文を用意
		String sql = "select * from category_table";

		//実行は一番最後に置くsetの前においてはいけない
		Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery(sql);


		while(rs.next()) {
			int id = rs.getInt("id");
			String categoryname = rs.getString("category_name");
			CategoryTableDataBeans category = new CategoryTableDataBeans(id,categoryname);
			categorylist.add(category);
		} return categorylist;

		}catch (SQLException e) {
		e.printStackTrace();
	    return null;
		}finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	   }
	 }
}
