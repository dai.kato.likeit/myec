package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.UserdetaBeans;

public class UserDAO {
	//新規登録をする為のメソッド
	public static void insertInfo(String userId,String userName,String userPassword,String userAdress) {
		Connection conn = null;
		try {
			 // データベースへ接続
	        conn = DBManager.getConnection();



	        // INSERT文でデータを入れる
	        String sql =  "insert into t_user (name,adress,login_id,login_password) values (?,?,?,?)";
	        //PreparedStatement
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setString(1, userId);
	        ps.setString(2, userName);
	        ps.setString(3, userPassword);
	        ps.setString(4, userAdress);

	        ps.executeUpdate();
			}catch (SQLException e) {
				e.printStackTrace();
			}finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }

	//ログインする為のメソッド
	public static UserdetaBeans loginbyId(String loginid,String password) {
		Connection con = null;
		try {
			// データベースへ接続
            con = DBManager.getConnection();

            //select文を実行
            String sql = "select * from t_user where login_id = ? and login_password = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, loginid);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            String adressData = rs.getString("adress");
            int userid = rs.getInt("id");
            return new UserdetaBeans(loginIdData,nameData,adressData,userid);

		 } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }
}





