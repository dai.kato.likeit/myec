package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDetaDetailBeans;
import beans.ItemdetaBeans;


public class BuyDetailDAO {

	public static void insertBuyDetail(BuyDetaDetailBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



/**
 * 購入IDによる購入詳細情報検索
 * @param buyId
 * @return buyDetailItemList ArrayList<ItemDataBeans>
 *             購入詳細情報のデータを持つJavaBeansのリスト
 * @throws SQLException
 */
public static ArrayList<ItemdetaBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement(
				"SELECT m_item.id,"
				+ " m_item.name,"
				+ " m_item.price"
				+ " FROM t_buy_detail"
				+ " JOIN m_item"
				+ " ON t_buy_detail.item_id = m_item.id"
				+ " WHERE t_buy_detail.buy_id = ?");
		st.setInt(1, buyId);

		ResultSet rs = st.executeQuery();
		ArrayList<ItemdetaBeans> buyDetailItemList = new ArrayList<ItemdetaBeans>();

		while (rs.next()) {
		int id = rs.getInt("id");
		String name = rs.getString("name");
		int price = rs.getInt("price");
		ItemdetaBeans idb = new ItemdetaBeans(id,name,price);

		buyDetailItemList.add(idb);
		}

		System.out.println("searching ItemDataBeansList by BuyID has been completed");
		return buyDetailItemList;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}
}
